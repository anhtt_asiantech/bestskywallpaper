package com.anhttvn.skyWallpaperHD.util;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.anhttvn.skyWallpaperHD.R;
import com.anhttvn.skyWallpaperHD.database.DatabaseHandler;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.annotations.NotNull;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author anhtt61
 * @version 1.1.1
 * @2022
 */
public abstract class BaseActivity extends AppCompatActivity {
  protected DatabaseHandler db;
  protected Bundle savedInstanceState;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    savedInstanceState = savedInstanceState;
    setContentView(contentView());
    db = new DatabaseHandler(this);
    init();
    configFullADS();
    createFolder();
  }

  public abstract void init();
  public abstract View contentView();


  protected void homeWallpaper(Bitmap bm){
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    int height = metrics.heightPixels;
    int width = metrics.widthPixels;
    Bitmap bitmap = Bitmap.createScaledBitmap(bm,width,height, true);
    WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
    wallpaperManager.setWallpaperOffsetSteps(1, 1);
    wallpaperManager.suggestDesiredDimensions(width, height);
    try {
      wallpaperManager.setBitmap(bitmap);
    } catch (IOException e) {
      e.printStackTrace();

    }
  }
  protected void lockWallpaper(Bitmap bm){
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    int height = metrics.heightPixels;
    int width = metrics.widthPixels;
    Bitmap bitmap = Bitmap.createScaledBitmap(bm,width,height, true);
    WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
    wallpaperManager.setWallpaperOffsetSteps(1, 1);
    wallpaperManager.suggestDesiredDimensions(width, height);
    try {
      wallpaperManager.setBitmap(bitmap);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  protected boolean isConnected() {
    return com.anhttvn.skyWallpaperHD.util.Connectivity.isConnectedFast(this);
  }

  /**
   * show ads banner
   */
  public void isBannerADS (AdView ads) {
    AdRequest adRequest = new AdRequest.Builder()
            .addTestDevice("2C995D2A909C1537C3C52A40B8DA69D9").build();
    if (isConnected()) {
      ads.setVisibility(View.VISIBLE);
      ads.loadAd(adRequest);
    }else{
      ads.setVisibility(View.GONE);
    }
  }

  private InterstitialAd mInterstitialAd;
  private AdRequest mAdRequest;

  protected void configFullADS() {
    MobileAds.initialize(getApplicationContext(),
            "ca-app-pub-3840180634112397~9187759690");

    mAdRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
            .addTestDevice("2C995D2A909C1537C3C52A40B8DA69D9").build();
    mInterstitialAd = new InterstitialAd(this);
    mInterstitialAd.setAdUnitId(this.getString(R.string.banner_full_screen));
    if(isConnected()){
      mInterstitialAd.loadAd(mAdRequest);
    }

  }

  protected void isADSFull() {
    if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
      mInterstitialAd.show();
    }
  }

  protected void downloadImage(String pathURL, final String name) {
    final ProgressDialog dialog = new ProgressDialog(this);
    dialog.setMessage(getString(R.string.please_download));
    dialog.show();
    Picasso.with(this)
            .load(pathURL)
            .into(new Target() {
              @Override
              public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                try {

                  File myDir = new File(getExternalFilesDir() + File.separator + Config.FOLDER_DOWNLOAD);

                  if (!myDir.exists()) {
                    myDir = new File(getExternalStorageDirectory() +File.separator+ Config.FOLDER_DOWNLOAD);
                    if (!myDir.exists()) {
                      askPermission();
                    }
                  }

                  String nameFile = name + ".jpg";
                  myDir = new File(myDir, nameFile);
                  FileOutputStream out = new FileOutputStream(myDir);
                  bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                  new android.os.Handler().postDelayed(
                          new Runnable() {
                            public void run() {
                              dialog.dismiss();
                              showToast(getString(R.string.download_success));
                            }
                          },
                          1000);

                  out.flush();
                  out.close();

                } catch(Exception e){
                  // some action
                }
              }

              @Override
              public void onBitmapFailed(Drawable errorDrawable) {

              }

              @Override
              public void onPrepareLoad(Drawable placeHolderDrawable) {

              }
            });
  }

  protected void showToast(String mToastMsg) {
    Toast.makeText(this, mToastMsg, Toast.LENGTH_LONG).show();
  }

  private void createFolder() {
    if (ContextCompat.checkSelfPermission(BaseActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
      createDirectory();

    }else{
      askPermission();
    }
  }

  private void createDirectory() {
    if (getExternalFilesDir() != null) {
      File folder = new File(getExternalFilesDir(), File.separator + Config.FOLDER_DOWNLOAD);
      if (!folder.exists()) {
        folder.mkdirs();
      }
    }else  {
      File myDir = new File(getExternalStorageDirectory() +File.separator+ Config.FOLDER_DOWNLOAD);

      if (!myDir.exists()) {
        myDir.mkdirs();
      }

    }
  }
  private String getExternalStorageDirectory() {
    String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
    return baseDir != null && baseDir.length() > 0 ?  baseDir : null;
  }

  private  File getExternalFilesDir() {
    File path = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    return path.getAbsolutePath() != null ? path : null;
  }
  private void askPermission() {

    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Config.PERMISSION_REQUEST_CODE);
  }

  protected List<String> getAllFileFolder() {
    List<String> list = new ArrayList<>();
    String path =
            getExternalFilesDir() != null ? getExternalFilesDir() +"/" +File.separator + "/" + Config.FOLDER_DOWNLOAD :
            getExternalStorageDirectory() +File.separator+ Config.FOLDER_DOWNLOAD;
    if (path == null) {
      return list;
    }
    File root =new File(path);
    File[] files= root.listFiles();
    list.clear();
    for (File file: files){
      list.add(file.getPath());
    }

    return list;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {

    if (requestCode == Config.PERMISSION_REQUEST_CODE)
    {
      if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        createDirectory();
      }else {
        Toast.makeText(BaseActivity.this,"Permission Denied",Toast.LENGTH_SHORT).show();
      }

    }

    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }
}