package com.anhttvn.skyWallpaperHD.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.skyWallpaperHD.R;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> implements View.OnClickListener {

  private List<String> images;
  private EventGallery eventGallery;
  private Context mContext;
  private String typeAdapter;
  public GalleryAdapter(Context context, List<String> list, String type, EventGallery click) {
    super();
    images = list;
    eventGallery = click;
    mContext = context;
    typeAdapter = type;
  }

  @NonNull
  @Override
  public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(mContext)
            .inflate(R.layout.item_wallpaper,parent,false);
    GalleryViewHolder itemView = new GalleryViewHolder(view);
    return itemView;
  }

  @Override
  public void onBindViewHolder(@NonNull GalleryViewHolder holder, int position) {
    if (typeAdapter.compareToIgnoreCase("folder") == 0) {
      File imgFile = new File(images.get(position));
      if(imgFile.exists())
      {
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        holder.image.setImageBitmap(myBitmap);
      }
    } else {
      InputStream inputstream= null;
      try {
        inputstream = mContext.getAssets().open(images.get(position));
      } catch (IOException e) {
        e.printStackTrace();
      }
      Drawable drawable = Drawable.createFromStream(inputstream, null);
      holder.image.setImageDrawable(drawable);
    }

    holder.image.setOnClickListener(this);
    holder.image.setTag(position);


  }

  @Override
  public int getItemCount() {
    return images.size();
  }

  @Override
  public void onClick(View v) {
    int position = Integer.parseInt(v.getTag()+"");
    switch (v.getId()){
      case R.id.item:
        notifyDataSetChanged();
        eventGallery.sendWallpaper(position);
        break;
    }
  }

  public class GalleryViewHolder extends RecyclerView.ViewHolder {
    private ImageView image;
    public GalleryViewHolder(@NonNull View itemView) {
      super(itemView);
      image = itemView.findViewById(R.id.item);
    }
  }

  public interface EventGallery {
    void sendWallpaper(int position);
  }
}
