package com.anhttvn.skyWallpaperHD.layout;

import android.app.ProgressDialog;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.anhttvn.skyWallpaperHD.R;
import com.anhttvn.skyWallpaperHD.databinding.ActivityPrivateBinding;
import com.anhttvn.skyWallpaperHD.util.BaseActivity;


public class PrivatePolice extends BaseActivity {

  private ActivityPrivateBinding privateBinding;
  @Override
  public void init() {
    if (getSupportActionBar() != null) {
      getSupportActionBar().hide();
    }
    isBannerADS(privateBinding.ads);
    privateBinding.title.setText(getString(R.string.privacyPolicy));

    ProgressDialog progressDialog = new ProgressDialog(this);
    progressDialog.setMessage("Loading");
    progressDialog.setCancelable(false);
    privateBinding.information.requestFocus();
    privateBinding.information.getSettings().setLightTouchEnabled(true);
    privateBinding.information.getSettings().setJavaScriptEnabled(true);
    privateBinding.information.getSettings().setGeolocationEnabled(true);
    privateBinding.information.setSoundEffectsEnabled(true);
    privateBinding.information.setBackgroundColor(0);
    progressDialog.show();

    privateBinding.information.loadUrl("file:///android_asset/private.html");
    privateBinding.information.requestFocus();
    privateBinding.information.setWebViewClient(new WebViewClient() {
      @Override
      public void onPageFinished(WebView view, String url) {
        progressDialog.dismiss();
      }
    });

  }

  @Override
  public View contentView() {
    privateBinding = ActivityPrivateBinding.inflate(getLayoutInflater());
    return privateBinding.getRoot();
  }

  public void onBack(View view) {
    finish();
  }
}
