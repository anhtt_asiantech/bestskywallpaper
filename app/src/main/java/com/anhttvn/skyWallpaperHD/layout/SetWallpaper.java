package com.anhttvn.skyWallpaperHD.layout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.anhttvn.skyWallpaperHD.R;
import com.anhttvn.skyWallpaperHD.databinding.ActivitySetWallpaperBinding;
import com.anhttvn.skyWallpaperHD.model.MessageEvent;
import com.anhttvn.skyWallpaperHD.model.Wallpaper;
import com.anhttvn.skyWallpaperHD.util.BaseActivity;
import com.anhttvn.skyWallpaperHD.util.Config;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @author anhtt61
 * @version 1.1.1
 * @2022
 */
public class SetWallpaper extends BaseActivity {
  private ActivitySetWallpaperBinding setWallpaperBinding;
  private Wallpaper wallpaper;
  private boolean isAllVisibleFab = false;

  @Override
  public void init() {
    getSupportActionBar().hide();
    setWallpaperBinding.fab.fabSetWallpaper.setVisibility(View.GONE);
    setWallpaperBinding.fab.fabDownload.setVisibility(View.GONE);
    setWallpaperBinding.fab.fabFavorite.setVisibility(View.GONE);

    isBannerADS(setWallpaperBinding.ads);
    getDataIntent();
    eventFab();
    changeIconFavorite(db.isFavoriteWallpaper(wallpaper.getId()));
  }


  private void eventFab() {
    setWallpaperBinding.fab.fabAdd.setOnClickListener(v -> {
      if (!isAllVisibleFab) {
        setWallpaperBinding.fab.fabAdd.setImageResource(R.drawable.ic_close);
        setWallpaperBinding.fab.fabSetWallpaper.show();
        setWallpaperBinding.fab.fabDownload.show();
        setWallpaperBinding.fab.fabFavorite.show();
        isAllVisibleFab = true;
      } else {
        setWallpaperBinding.fab.fabAdd.setImageResource(R.drawable.icon_add);
        setWallpaperBinding.fab.fabSetWallpaper.hide();
        setWallpaperBinding.fab.fabDownload.hide();
        setWallpaperBinding.fab.fabFavorite.hide();
        isAllVisibleFab = false;
      }
    });

    setWallpaperBinding.fab.fabSetWallpaper.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      ProgressDialog dialog = new ProgressDialog(this);
      dialog.setMessage(getString(R.string.please_set_wallpaper));
      dialog.show();
      Picasso.with(this)
              .load(wallpaper.getPath())
              .into(new Target() {
                @Override
                public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                  homeWallpaper(bitmap);
                  lockWallpaper(bitmap);
                  new android.os.Handler().postDelayed(
                          () -> {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            dialog.dismiss();
                            finish();
                          },
                          1000);

                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                  dialog.dismiss();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
              });
    });

    setWallpaperBinding.fab.fabDownload.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      downloadImage(wallpaper.getPath(), wallpaper.getTitle());
    });

    setWallpaperBinding.fab.fabFavorite.setOnClickListener(v -> {
      changeFavorite();
    });
  }

  private void changeIconFavorite(boolean is) {
    setWallpaperBinding.fab.fabFavorite.setImageResource(is ? R.drawable.ic_favorite_disabel : R.drawable.icon_favorite_disable);
  }

  private void changeFavorite() {
    boolean isExits = db.isFavoriteWallpaper(wallpaper.getId());
    if (isExits) {
      changeIconFavorite(false);
      wallpaper.setFavorite(0);

    } else {
      changeIconFavorite(true);
      wallpaper.setFavorite(1);
    }
    db.updateWallpaper(wallpaper);
  }


  @Override
  public View contentView() {
    setWallpaperBinding = ActivitySetWallpaperBinding.inflate(getLayoutInflater());
    return setWallpaperBinding.getRoot();
  }

  private void getDataIntent() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    wallpaper = (Wallpaper) bundle.getSerializable("wallpaper");
    if (wallpaper != null) {
      setWallpaperBinding.title.setText(R.string.app_name);
      Picasso.with(getApplicationContext()).load(wallpaper.getPath())
              .placeholder(R.drawable.ic_no_thumbnail)
              .error(R.drawable.ic_no_thumbnail)
              .into(setWallpaperBinding.imgWallpaper);
      wallpaper.setView(wallpaper.getView() + 1);
      db.updateWallpaper(wallpaper);
    }

  }


  public void onBack(View view) {
    onBackPressed();
  }

  @Override
  public void onBackPressed() {
    EventBus.getDefault().postSticky(new MessageEvent("UPDATE"));
    isADSFull();
    finish();
    super.onBackPressed();
  }
}
