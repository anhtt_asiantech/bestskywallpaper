package com.anhttvn.skyWallpaperHD.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.anhttvn.skyWallpaperHD.model.Wallpaper;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {
  public DatabaseHandler(Context context) {
    super(context, ConfigData.DATABASE, null, ConfigData.VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(ConfigData.CREATE_TABLE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL(ConfigData.DELETE_TABLE);
    onCreate(db);
  }

  public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    onUpgrade(db, oldVersion, newVersion);
  }

  public void addWallpaper(Wallpaper wallpaper) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(ConfigData.ID, wallpaper.getId());
    values.put(ConfigData.TITLE, wallpaper.getTitle());
    values.put(ConfigData.PATH, wallpaper.getPath());
    values.put(ConfigData.VIEW, wallpaper.getView());
    values.put(ConfigData.FAVORITE, wallpaper.getFavorite());
    db.insert(ConfigData.TABLE, null, values);
    db.close();
  }

  public void updateWallpaper(Wallpaper wallpaper) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(ConfigData.TITLE, wallpaper.getTitle());
    values.put(ConfigData.PATH, wallpaper.getPath());
    values.put(ConfigData.VIEW, wallpaper.getView());
    values.put(ConfigData.FAVORITE, wallpaper.getFavorite());
    db.update(ConfigData.TABLE, values, ConfigData.ID + "=?", new String[]{wallpaper.getId()});
    db.close();
  }


  public boolean isWallpaper(String id) {
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(ConfigData.TABLE, null,
            ConfigData.ID + " = ?", new String[]{String.valueOf(id)},
            null, null, null);
    cursor.moveToFirst();
    int count = cursor.getCount();
    cursor.close();
    return count > 0;
  }


  public boolean isFavoriteWallpaper(String id) {
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(ConfigData.TABLE, null,
            ConfigData.ID + " =?" + " AND " + ConfigData.FAVORITE + " =?",
            new String[]{String.valueOf(id), String.valueOf(1)},
            null, null, null);
    cursor.moveToFirst();
    int count = cursor.getCount();
    cursor.close();
    return count > 0;
  }

  public int countView(String id) {
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.query(ConfigData.TABLE, null,
            ConfigData.ID + " = ?", new String[]{String.valueOf(id)},
            null, null, null);
    cursor.moveToFirst();
//    int count = cursor.getInt(cursor.getColumnIndex(ConfigData.VIEW));
//    cursor.close();
    Wallpaper wallpaper = new Wallpaper();
    while (!cursor.isAfterLast()) {
      wallpaper.setId(cursor.getString(0));
      wallpaper.setTitle(cursor.getString(1));
      wallpaper.setPath(cursor.getString(2));
      wallpaper.setFavorite(cursor.getInt(3));
      wallpaper.setView(cursor.getInt(4));
      cursor.moveToNext();
    }
    cursor.close();
    return wallpaper.getView();
  }

  public ArrayList<Wallpaper> listWallpaper() {
    ArrayList<Wallpaper> list = new ArrayList<>();
    String query = "SELECT * FROM " + ConfigData.TABLE;
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(query, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      Wallpaper wallpaper = new Wallpaper();
      wallpaper.setId(cursor.getString(0));
      wallpaper.setTitle(cursor.getString(1));
      wallpaper.setPath(cursor.getString(2));
      wallpaper.setFavorite(cursor.getInt(3));
      wallpaper.setView(cursor.getInt(4));

      list.add(wallpaper);
      cursor.moveToNext();
    }
    cursor.close();
    return list;
  }

  public ArrayList<Wallpaper> listFavoriteWallpaper() {
    ArrayList<Wallpaper> list = new ArrayList<>();
    String query = "SELECT * FROM " + ConfigData.TABLE +" WHERE " +ConfigData.FAVORITE + "='1'";
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(query, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      Wallpaper wallpaper = new Wallpaper();
      wallpaper.setId(cursor.getString(0));
      wallpaper.setTitle(cursor.getString(1));
      wallpaper.setPath(cursor.getString(2));
      wallpaper.setFavorite(cursor.getInt(3));
      wallpaper.setView(cursor.getInt(4));

      list.add(wallpaper);
      cursor.moveToNext();
    }
    cursor.close();
    return list;
  }

}