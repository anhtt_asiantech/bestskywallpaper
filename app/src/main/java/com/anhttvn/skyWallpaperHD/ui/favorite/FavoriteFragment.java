package com.anhttvn.skyWallpaperHD.ui.favorite;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.skyWallpaperHD.R;
import com.anhttvn.skyWallpaperHD.adapter.PhotoAdapter;
import com.anhttvn.skyWallpaperHD.database.ConfigData;
import com.anhttvn.skyWallpaperHD.database.DatabaseHandler;
import com.anhttvn.skyWallpaperHD.databinding.FragmentFavoriteBinding;
import com.anhttvn.skyWallpaperHD.layout.SetWallpaper;
import com.anhttvn.skyWallpaperHD.model.Wallpaper;
import com.anhttvn.skyWallpaperHD.util.BaseFragment;

import java.util.ArrayList;
import java.util.List;
/**
 * @author anhtt61
 * @version 1.1.1
 * @2022
 */
public class FavoriteFragment extends BaseFragment implements PhotoAdapter.OnclickImage {
  private FragmentFavoriteBinding favoriteBinding;
  private PhotoAdapter mPhotoAdapter;
  private List<Wallpaper> listImage = new ArrayList<>();

  @Override
  protected View initView(LayoutInflater inflater, ViewGroup container, boolean b) {
    favoriteBinding = FragmentFavoriteBinding.inflate(inflater, container, b);
    return favoriteBinding.getRoot();
  }

  @Override
  protected void init() {

  }

  private void initAdapter() {
    if (listImage != null && listImage.size() > 0) {
      mPhotoAdapter = new PhotoAdapter(getActivity(), listImage, this);
      RecyclerView.LayoutManager layoutManager =
              new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
      favoriteBinding.listRecent.setLayoutManager(layoutManager);
      favoriteBinding.listRecent.setItemAnimator(new DefaultItemAnimator());
      favoriteBinding.listRecent.setAdapter(mPhotoAdapter);
      mPhotoAdapter.notifyDataSetChanged();
    }
  }

  @Override
  public void selectedPosition(int position) {
    if (isConnected()) {
      Intent intent = new Intent(getActivity(), SetWallpaper.class);
      intent.putExtra("wallpaper", listImage.get(position));
      startActivity(intent);
    } else {
      Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
    }

  }

  @Override
  public void onResume() {
    super.onResume();
    db = new DatabaseHandler(getActivity());
    listImage = db.listFavoriteWallpaper();
    favoriteBinding.progressBar.setVisibility(View.VISIBLE);
    favoriteBinding.listRecent.setVisibility(View.GONE);
    favoriteBinding.noData.getRoot().setVisibility(View.GONE);
    if (listImage.size() > 0) {
      favoriteBinding.progressBar.setVisibility(View.GONE);
      favoriteBinding.listRecent.setVisibility(View.VISIBLE);
      initAdapter();
    } else {
      favoriteBinding.noData.getRoot().setVisibility(View.VISIBLE);
      favoriteBinding.progressBar.setVisibility(View.GONE);
    }

  }
}
